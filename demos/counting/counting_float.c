#include <stdio.h>

// Not lectured: This demonstrates what happens when we try to keep incrementing a 'float' forever.  After a certain point, we see no further increase owing to underflow.
// We should expect the complete underflow to occur at about 10^7.  
// It will print this output  1.67772160e+07 (0x4b800000 or 151:000000)

static void showval(float f)
{   union { float f; int n; } u;
    u.f = f;
    /* This use of 'union' is *not* portable, but it works on x86/gcc.  */
    printf("%.8e (0x%.8x or %d:%.6x)\n", f, u.n,
           u.n>>23 & 0xff, u.n & 0x7fffff);
}

int main()
{ float i=0,j;
  do { j=i; i++; } while (i != j);
  showval(i);
  return 0;
}

//eof
