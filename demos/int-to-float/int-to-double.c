// Convert an integer to a double.

// (C) 1998 DJ Greaves, University of Cambridge Computer Laboratory.

# include <stdio.h>

double int_to_double_builtin(int arg)
{
  double r = (double) arg; 
  return r;
}

double int_to_double_manual(long long int arg)
{

  union 
  {
    double f; long long int i;
  } rr;
  rr.i = 0;
  if (arg==(1LL<<63)) 
    {
      rr.i = 0xBFFL<<52L;
      // special case
    }
  else if (arg != 0)
    {
      unsigned long long sign_bit = (arg <0) ? 1L<<63L: 0;
      arg = abs(arg);
      int exp = 1023+52;
      if (arg == (1L<<52L))
	{
	  // nothing to do.
	}
      else if (arg < (1L<<52L))
	{
	  while(arg < (1L<<52L))
	    {
	      arg <<= 1;
	      exp -= 1;
	    }
	}
      else
	{
	  while(arg >= (1L<<52L))
	    {
	      arg >>= 1;
	      exp += 1;
	      //printf (" a=%08x  e=%x\n", arg, exp);
	    }
	  arg <<= 1;
	  exp -=1;
	}
      rr.i |= arg & ((1L<<52L)-1);
      rr.i |= (unsigned long long)(exp) << 52L;
      rr.i |= sign_bit;
    }
  return rr.f;
}



void test_one_value(long long int p)
{
  double b = int_to_double_builtin(p);
  double m = int_to_double_manual(p);

  union 
  {
    double f; long long int i;
  } bb, mm;
  bb.f = b;
  mm.f = m;
  
  printf("test_one_value %lli -> %f or %f   (%16llx cf %16llx)  %s\n", p, b, m, bb.i, mm.i, (b==m)?"":" WRONG !");
}

int main()
{
  test_one_value(1);
  test_one_value(5);
  test_one_value(40);
  test_one_value(1234567);
  test_one_value(-10000);
  printf("\n");
  test_one_value(0); //Special case 0
  test_one_value(0x8000<<56); // Second special case
  printf("\n");
  test_one_value(0x8000<<14);
  test_one_value(0x8020<<14);

  test_one_value(0x1234567);
  return 0;

}
// eof
