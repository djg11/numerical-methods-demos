// Convert an integer to a float.

// (C) 1998 DJ Greaves, University of Cambridge Computer Laboratory.

# include <stdio.h>

float int_to_float_builtin(int arg)
{
  float r = (float) arg; 
  return r;
}

float int_to_float_manual(int arg)
{

  union 
  {
    float f; int i;
  } rr;
  rr.i = 0;
  if (arg==(1<<31)) 
    {
      rr.i = 0xCF<<24;
      // special case
    }
  else if (arg != 0)
    {
      unsigned int sign_bit = (arg <0) ? 1<<31: 0;
      arg = abs(arg);
      int exp = 128+22;
      if (arg == (1<<23))
	{
	  // nothing to do.
	}
      else if (arg < (1<<23))
	{
	  while(arg < (1<<23))
	    {
	      arg <<= 1;
	      exp -= 1;
	    }
	}
      else
	{
	  while(arg >= (1<<23))
	    {
	      arg >>= 1;
	      exp += 1;
	      //printf (" a=%08x  e=%x\n", arg, exp);
	    }
	  arg <<= 1;
	  exp -=1;
	}
      rr.i |= arg & ((1<<23)-1);
      rr.i |= exp << 23;
      rr.i |= sign_bit;
    }
  return rr.f;
}



void test_one_value(int p)
{
  float b = int_to_float_builtin(p);
  float m = int_to_float_manual(p);

  union 
  {
    float f; int i;
  } bb, mm;
  bb.f = b;
  mm.f = m;
  
  printf("test_one_value %i -> %f or %f   (%08x cf %08x)  %s\n", p, b, m, bb.i, mm.i, (b==m)?"":" WRONG !");
}

int main()
{
  test_one_value(4);
  test_one_value(40);
  test_one_value(1234567);
  test_one_value(-10000);
  printf("\n");
  test_one_value(0); //Special case 0
  test_one_value(0x8000<<16); // Second special case
  printf("\n");
  test_one_value(0x8000<<14);
  test_one_value(0x8020<<14);

  test_one_value(0x1234567); // Demonstrate lack of rounding!
  test_one_value(0x1234566);
  test_one_value(0x1234565);
  test_one_value(0x1234564);
  test_one_value(0x1234563);
  return 0;

}
// eof
