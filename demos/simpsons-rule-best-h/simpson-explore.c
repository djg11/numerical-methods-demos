//
// Simpson's Rule for Quadrature - We explore the best setting of h.  We find it is at roughly the fifth root of macheps.
//
// University of Cambridge Computer Laboratory - Numerical Methods Demos
// (C) DJ Greaves, 2015. 



#include <stdio.h>
#include <math.h>


#define ACC_t float // Computer DUT values with this type.

int func = 0;
int rule_o = -1;


// The function that is the 'device under test' dut.
ACC_t dutf(double arg)
{
  if (func == 1) return arg *arg;
  return sin(arg);
}

// The integral for the dut - of course we do not have access to this in general, since if we did
// we would not bother with quadrature.
double dutf_integrated(double arg)
{
  if (func == 1) return arg*arg*arg/3.0;

  return -cos(arg);
}



// Rule paramater instructs what sort of quadrature to use - 2 is Simpson's Rule
// h0 is the nominal strip width
void strips(int rule, int steps)
{
  int i;

  // We compute the definite integral between these two limits
  ACC_t start = 0.5;
  ACC_t end = 1.5;
  double true_answer = dutf_integrated(end) - dutf_integrated(start);
  //int steps = (int) ((end-start)/h0);
  // Steps must be odd for Simpsons Rule, and number of strips is steps-1, which is even.
  steps &= ~1; // - Convert to odd number.
  ACC_t h = (end-start)/((float)steps);
  
  ACC_t xf = (end-start)/(ACC_t)steps;
  ACC_t ans = 0;
  ACC_t ss = 0.0;
  if (rule==0)
    { if (rule!=rule_o) { rule_o = rule; printf("Riemann Sums\n"); }
      for (i=0; i<steps; i++) // Note for-loop exit conditions vary per rule.
	{
	  ACC_t x = start + (xf * (ACC_t)i);
	  ACC_t y = dutf((double)x);
	  ss += y;
	}
      ans = ss * h;
    }
  else if (rule==1)
    { if (rule!=rule_o) { rule_o = rule; printf("MidPointRule - Trapezoidal Strips\n"); }
      for (i=0; i<=steps; i++)
	{
	  ACC_t x = start + (xf * ((ACC_t)i + 0.5));
	  ACC_t y = dutf((double)x);  // A better implementation would not call this twice.
	  if (i==0 || i==steps-1) ss += y;
	  else ss += 2*y;
	}
      ans = ss * h * 0.5;
    }
  else if (rule==2)
    {
      if (rule!=rule_o) { rule_o = rule; printf("Simpson's Rule\n"); }
      for (i=0; i<=steps; i++) // Include both end points for Simpson's rule.
	{
	  ACC_t x = start + (xf * (ACC_t)i);
	  ACC_t y = dutf((double)x);
	  //printf("  datum for simpsons %e -> %e\n", x, y);
	  if (i==0 || i==steps) ss += y;
	  else if (i & 1) ss += 4*y;  // Odd points 4 times.
	  else ss += 2*y; // Even points twice. 
	}
      //ans = ss / (ACC_t)(3 * (steps));
      ans = ss * h / 3.0; // Assume divide-by-constant is optimised to a multiply by compiler.
    }
  double rel_error = (ans-true_answer)/true_answer;
  printf("%i steps, for %e  \t result=%e \t rel_error=%e \n", steps, h, ans, rel_error);

  // Use the following print for output to a gnuplot-friendly file.
  // printf(" %e %e \n", h, rel_error);  
}


int testbench_old()
{
  printf("Start Testbench Old\n");
  int r;
  float hbest = 3.1e-4;
  for (func=0; func<2; func++)
    {
      printf("Function=%i\n", func);
      for (r=0; r<3;r++)
	{
	  strips(r, 500);
	}
    }
  return 0;
}


int main()
{
  testbench_old();
  printf("Start testbench\n");
  int rule = 2;
  double h;

  for (func=0; func<2; func++)
    {
      printf("Function=%i\n", func);
      for (h =0.3;h>1.0e-7; h *= 0.7)
	{
	  strips(rule, 1.0/h);
	}
    }
  return 0;
}


// eof
